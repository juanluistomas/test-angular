import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketDataFieldComponent } from './market-data-field.component';

describe('MarketDataFieldComponent', () => {
  let component: MarketDataFieldComponent;
  let fixture: ComponentFixture<MarketDataFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarketDataFieldComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketDataFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
