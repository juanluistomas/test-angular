import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-market-data-field',
  templateUrl: './market-data-field.component.html',
  styleUrls: ['./market-data-field.component.scss']
})
export class MarketDataFieldComponent implements OnInit {

  @Input('label') label: string | undefined;
  @Input('value') value: string | number | undefined;
  @Input('valueSuffix') valueSuffix: string | null = null;
  @Input('colorValue') colorValue: boolean = false;
  @Input('date') date?: Date | undefined;
  @Input('dateFormat') dateFormat?: string = 'dd/MM/YYYY';

  constructor() { }

  ngOnInit(): void {
  }

  get formatValue() {
    return this.colorValue && this.value !== undefined && typeof this.value === 'number';
  }

}
