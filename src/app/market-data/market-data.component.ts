import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {MarketDataService} from './market-data.service';
import {catchError, filter, finalize, first, Subject, switchMap, tap} from 'rxjs';
import {ApiDataQuote, ApiDataResponse} from './market-data.types';

@Component({
  selector: 'app-market-data',
  templateUrl: './market-data.component.html',
  styleUrls: ['./market-data.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MarketDataComponent implements OnInit, OnDestroy {

  isLoading: boolean = true;
  errorLoading: boolean = false;

  quotes: ApiDataQuote[] = [];

  private _unsubscribeAll: Subject<void> = new Subject<void>();

  constructor(
    private dataService: MarketDataService,
    private _ch: ChangeDetectorRef
  ) { }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  ngOnInit(): void {
    this.dataService.hasToken$.pipe(
      filter((val: boolean) => val),
      first(),
      switchMap(() => this.dataService.getData('2970161-1058-814', 'LVAL_NORM,CLOSE_ADJ_NORM,NC2_PR_NORM,NC2_NORM,VOL,TUR,PY_CLOSE,YTD_PR_NORM')),
      tap((res: ApiDataResponse) => this.quotes = res.quotes),
      catchError(this.catchError),
      finalize(() => {
        this.isLoading = false;
        this._ch.markForCheck();
      }),
    ).subscribe();
    this.login();
  }

  login() {
    this.dataService.login().pipe(first(),
      catchError(this.catchError),
    ).subscribe();
  }

  catchError(err: any) {
    /* Handle error */
    console.error(err);
    this.errorLoading = true;
    this.isLoading = false;
    this._ch.markForCheck();
    return err;
  }

  trackByFn(index: number, item: ApiDataQuote) {
    return item.listingKey;
  }
}
