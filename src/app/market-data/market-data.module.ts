import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarketDataRoutingModule } from './market-data-routing.module';
import { MarketDataComponent } from './market-data.component';
import { MarketDataFieldComponent } from './market-data-field/market-data-field.component';


@NgModule({
  declarations: [
    MarketDataComponent,
    MarketDataFieldComponent
  ],
  imports: [
    CommonModule,
    MarketDataRoutingModule
  ]
})
export class MarketDataModule { }
