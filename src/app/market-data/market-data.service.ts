import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {API_CREDENTIALS} from './market-data.consts';
import {BehaviorSubject, map, Observable, of, ReplaySubject, Subject, tap} from 'rxjs';
import {ApiDataResponse, ApiTokenResponse} from './market-data.types';

@Injectable({
  providedIn: 'root'
})
export class MarketDataService {

  private base: string = 'https://integra1.solutions.webfg.ch/restweb';

  private token: string | undefined;
  private _hasToken: BehaviorSubject<any> = new BehaviorSubject(false);
  private _data: Subject<any> = new Subject();

  hasToken$ = this._hasToken.asObservable();
  data$ = this._data.asObservable();

  constructor(
    private http: HttpClient
  ) {
    // this._hasToken.next(false);
  }

  login() {
    const credentials = {
      grant_type: 'password',
      username: API_CREDENTIALS.username,
      scope: 'uaa.user',
      password: API_CREDENTIALS.password,
    };
    const headers = {
      Authorization: API_CREDENTIALS.authorization,
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    return this.http.post<ApiTokenResponse>(this.base + '/oauth/token', {}, {headers: headers, params: credentials})
      .pipe(tap((res: ApiTokenResponse) => {
        console.log(res);
        this.token = res.access_token;
        this._hasToken.next(true);
      }));
  }

  getData(quote: string, fields: string): Observable<ApiDataResponse> {
    if (!this.token) {
      throw new Error('Missing token');
    }
    const params = {
      fields: fields,
    };
    const headers = {
      Accept: 'application/vnd.solid-v1.0+json',
      'Accept-Encoding': 'gzip, deflate',
      Authorization: 'Bearer ' + this.token,
    };
    return this.http.get<ApiDataResponse>(this.base + '/quotes/' + quote, {headers: headers, params: params});
  }
}
