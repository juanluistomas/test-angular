export interface ApiCredentials {
  username: string;
  password: string;
  clientId: string;
  clientPassword: string;
  authorization: string;
}

export interface ApiTokenResponse {
  access_token: string;
  token_type: string;
  refresh_token: string;
  expires_in: string;
  scope: string;
}

export interface ApiDataResponse {
  links: any;
  quotes: ApiDataQuote[];
}

export interface ApiDataQuote {
  fields: {
    CLOSE_ADJ_NORM: ApiDataQuoteField,
    LVAL_NORM: ApiDataQuoteField,
    NC2_NORM: ApiDataQuoteField,
    NC2_PR_NORM: ApiDataQuoteField,
    PY_CLOSE: ApiDataQuoteField,
    TUR: ApiDataQuoteField,
    VOL: ApiDataQuoteField,
    YTD_PR_NORM: ApiDataQuoteField,
  };
  links: any;
  listingKey: string;
}

export interface ApiDataQuoteField {
  d: Date;
  dly: number;
  gen: number;
  v: number;
}
